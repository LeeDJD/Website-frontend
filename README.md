<div align="center">
    <h1>~ Website-Frontend ~</h1>
    <strong>Source code of my personal website</strong><br><br>
    <img src="https://forthebadge.com/images/badges/made-with-vue.svg" height="30" />&nbsp;
</div>

This is the source code of my [personal website](https://kappes.space). It uses a rather simple design and I am not really satisfied by the look of it. A rework is planned but for now I do not think I can realize my ambitions. For now the old version, the current master-branch, stays deployed.

## Installation

Download it via git and then start the [nuxt.js](https://nuxtjs.org) server.

```sh
git clone https://github.com/leonkappes/Website-Frontend.git .
npm install
npm run build
npm run start
```

## Contributing

 Pull requests are welcome. For major changes, please open an issue and state your changes.

 Please make sure that you dont change the project structure which I'd like to keep!

## License

This project is licensed under the [AGPL-3.0](https://choosealicense.com/licenses/agpl-3.0) license.
